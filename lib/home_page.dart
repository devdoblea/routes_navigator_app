import 'package:flutter/material.dart';
import 'package:routes_navigator_app/route_generator.dart';
//import 'package:routes_navigator_app/detail_page.dart';

class MyHomePage extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text('Routing App'),
			),
			body: Center(
				child: Column(
					mainAxisSize: MainAxisSize.min,
					children: <Widget> [
						Text(
							' First Page ',
							style: TextStyle(fontSize: 50),
						),
						RaisedButton(
							child: Text('Go to Second'),
							onPressed: () {
								Navigator.of(context).pushNamed(
									'/second',
									arguments:'Saludos desde la primera pagina',
								);
							}
						),
					],
				),
			),
		);
	}
}

// Con esta clase se puede hacer uso muy basico el ruteo de las paginas
// En la clase anterior a esta esta la forma recomendad de hacerlo
// Recordar descomentar el import de la pagina detail_page para usar esta clase
// class MyHomePage extends StatelessWidget {
// 	@override
// 	Widget build(BuildContext context) {
// 		return Scaffold(
// 			appBar: AppBar(
// 				title: Text('Routing App'),
// 			),
// 			body: Center(
// 				child: Column(
// 					mainAxisSize: MainAxisSize.min,
// 					children: <Widget> [
// 						Text(
// 							' First Page ',
// 							style: TextStyle(fontSize: 50),
// 						),
// 						RaisedButton(
// 							child: Text('Go to Second'),
// 							onPressed: () {
// 								// pasar una ruta directamente sin usar un nombre de ruta
// 								Navigator.of(context).push(
// 									// Con MaterialPageRoute se puede pasar datos entre paginas
// 									// pero si quieres mas complejidad esta via no funciona
// 									MaterialPageRoute(
// 										builder: (context) => SecondPage(data:'Saludos desde la primera pagina'),
// 									),
// 								);
// 							}
// 						),
// 					],
// 				),
// 			),
// 		);
// 	}
// }