import 'package:flutter/material.dart';
import 'package:routes_navigator_app/route_generator.dart';
import 'package:routes_navigator_app/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rutas con Flutter',
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],
        accentColor: Colors.cyan[600],
        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          body1: TextStyle(fontSize: 14.0, fontFamily: 'Roboto'),
        ),
      ),
      initialRoute: '/',
      onGenerateRoute: RouteGenerator.generateRoute,
      home: MyHomePage(),
    );

    // De esta forma se puede utilizar de manera muy basica el ruteo 
    // hacialas paginas. En el return arriba de este es como se 
    // recomienda hacer.
    // return MaterialApp(
    //   title: 'Rutas con Flutter',
    //   theme: ThemeData(
    //     // Define the default brightness and colors.
    //     brightness: Brightness.dark,
    //     primaryColor: Colors.lightBlue[800],
    //     accentColor: Colors.cyan[600],
    //     // Define the default TextTheme. Use this to specify the default
    //     // text styling for headlines, titles, bodies of text, and more.
    //     textTheme: TextTheme(
    //       headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
    //       title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
    //       body1: TextStyle(fontSize: 14.0, fontFamily: 'Roboto'),
    //     ),
    //   ),
    //   home: MyHomePage(),
    // );
  }
}
