import 'package:flutter/material.dart';
import 'package:routes_navigator_app/main.dart';
import 'package:routes_navigator_app/home_page.dart';
import 'package:routes_navigator_app/detail_page.dart';

class RouteGenerator {
	
	static Route<dynamic> generateRoute(RouteSettings settings) {
		//Preparando los argumantos pasados mientras se llama al Navigator.pushNamed
		final args = settings.arguments;

		switch(settings.name) {
			
			case '/':
				// Retornamos una ruta a la pagina principal
				return MaterialPageRoute(builder: (_) => MyHomePage());
			
			case '/second':
				// Validamos que venga la data correcta
				if(args is String){
					// y retornamos una ruta a la segunda pagina
					return MaterialPageRoute(
						builder: (_) => DetailPage(
							data: args,
						),
					);
				}
				// si args no es del tipo correcto retorna un error_page
				// tambien se puede disparar un Exception en modo desarrolllo
				return _errorRoute();

			default:
				// Si no hay una ruto nombrada enviada al switch, tambien da error
				return _errorRoute();

		}
	}

	static Route<dynamic> _errorRoute() {
		return MaterialPageRoute(builder: (_) {
			return Scaffold(
				appBar: AppBar(
					title: Text('Error'),
				),
				body: Center(
					child:Text('ERROR'),
				),
			);
		});
	}
}