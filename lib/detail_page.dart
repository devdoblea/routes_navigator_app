import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {

	// Esta es una variable tipo texto sencilla para recibir la data que viene
	// pero se debe cambiar para recibir exactamente el tipo de valor que venga
	final String data;

	DetailPage({
		Key key,
		@required this.data,
	}) : super(key: key);

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text("Routing App (detail)"),
			),
			body: Center(
				child: Column(
					mainAxisSize: MainAxisSize.min,
					children: <Widget> [
						Text(
							' Detail Page ',
							style: TextStyle(fontSize: 50),
						),
						Text(
							data,
							style: TextStyle(fontSize: 20),
						),
					],
				),
			),
		);
	}
}